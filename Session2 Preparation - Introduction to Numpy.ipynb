{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numpy\n",
    "\n",
    "\n",
    "Numpy is the core library for scientific computing in Python. It provides a high-performance multidimensional array object, and tools for working with these arrays.\n",
    "\n",
    "To use Numpy, we first need to import the `numpy` package.\n",
    "It is conventional to refer to numpy by the abbreviation 'np'.\n",
    "This will save you a lot of typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arrays\n",
    "A numpy array is a collection of values, _all of the same type_, placed in a grid that has one or more dimensions.\n",
    "\n",
    "Numpy arrays can be initialized from python collections, such as a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1,2,3])\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multidimensional numpy arrays are initialized from nested Python lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([[1, 2], [3, 4]])  # Create a 2-D array\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# type of the elements (data type):\n",
    "a.dtype"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# shape:\n",
    "a.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing\n",
    "Indexing a numpy array is similar to indexing lists (as we have seen in session 1).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# an index per dimension\n",
    "a[0, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# you can use 'slice' notation as 'start:stop:step':\n",
    "a[::-1, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# one index selects the outer (first) dimension:\n",
    "a[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# the above statement is the same as:\n",
    "a[1, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# you can use a tuple/list/array instead of a single index: \n",
    "a[0, (0, 1, 0, 1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# or both indices: \n",
    "a[(0, 0, 0, 0), (0, 1, 0, 1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# logical indexing:\n",
    "a[a > 1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# change a single element of the array\n",
    "a[1, 1] = 1\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# all elements of the array can be changed simultaneously\n",
    "a[:] = 5\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# change all element of a row of the array\n",
    "a[0, :] = 3\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use arithmatics on arrays, more or less like on scalar variables, and using numpy arrays makes your code run very fast:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a list with 1000 elements\n",
    "x = list(range(1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%timeit\n",
    "# 'square' all elements in the list and list the required time\n",
    "y = [i**2 for i in x]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a numpy array with 1000 elements\n",
    "x = np.arange(1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%timeit\n",
    "# 'square' all elements in the numpy array and list the required time\n",
    "y = x**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numpy also provides many functions to create arrays without too much typing.  \n",
    "Mind the double parenthesis!:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array of all zeros (floats by default)\n",
    "np.zeros((2, 2))           "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array of all integer ones\n",
    "np.ones((1, 2), dtype=int) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array where all elements are identical\n",
    "np.full((2, 2), 7)         "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a 2x2 identity matrix\n",
    "np.eye(2)                  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array filled with random values\n",
    "np.random.random((2, 2))   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array of three equal-length intervals between 0 and 2\n",
    "np.linspace(0, 2, 3, endpoint=False) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`r_` can be used to concatenate or create arrays with very little typing.  \n",
    "This can come in handy, but can make the code hard to read. Use it with care!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.r_[0:10:2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.r_[[[1, 2]], [[3, 4]]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.r_[[1, 2], [3, 4]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.r_[a, np.eye(2)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case you are familiar with MATLAB, there are a few subtle differences to watch out for:\n",
    "* Operators work element-wise by default\n",
    "* Order of indexing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# element-wise multiplication\n",
    "a * np.eye(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# matrix multiplication\n",
    "a @ np.eye(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For higher-dimensional arrays, the matrix multiplication is done over the last two dimensions.\n",
    "\n",
    "For one-dimensional arrays, `@` gives the inner product (sum of the element-wise products):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([1, 2]) @ np.array([1, 2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reshaping 'folds' an array into another shape. Numpy uses 'C-style' indexing (inner dimension last).\n",
    "To use Fortran-style (like MATLAB does), you can use `np.arange(24).reshape(2, 3, -1, order='F')`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -1 means numpy will find out how long this dimension needs to be\n",
    "np.arange(24).reshape(2, 3, -1)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many other ways to manipulate arrays. Have a look at the options given by the autocompletion: `x.<tab>` or `np.<tab>`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.cross(np.array([1, 0, 0]), np.array([0, 1, 0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are already familiar with MATLAB, you might find this [tutorial](http://wiki.scipy.org/NumPy_for_Matlab_Users) useful to get started with Numpy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scipy and Matplotlib\n",
    "\n",
    "The library Scipy holds lots of functionality for working with numpy arrays:   \n",
    "https://www.scipy.org/docs.html\n",
    "\n",
    "* usually you only import the sublibrary you need\n",
    "* Scipy uses multithreading for most of its functions, reducing the computational time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that all processors are in use (using your system monitor):\n",
    "from scipy import linalg  # linalg is a library, not a function\n",
    "\n",
    "linalg.inv(np.random.random((2**12, 2**12)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you generate larger sets of numerical output, it is convenient to view the output graphically.\n",
    "Python has many graphical libraries, For a start, you can have a look at the gallery of examples of Matplotlib:  \n",
    "https://matplotlib.org\n",
    "\n",
    "After a while you will know the most common ones by heart, but very often data visualisation starts by copy-parting something that looks similar to what you want and then adept it to your needs.\n",
    "\n",
    "Have fun experimenting and see you next session!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  },
  "nav_menu": {},
  "toc": {
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 6,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
