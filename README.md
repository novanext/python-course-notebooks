# python-course-notebooks

This repository contains supplementary material to the python course offered by novanext.

You can get started with the tutorials in this repository by clicking on the "launch binder" button below. This will launch an interactive python sandbox. The jupyter notebook home page will show up in a new tab. Click on any of the .ipynb files to launch the notebook file.

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/novanext%2Fpython-course-notebooks/master)


## Instructions for installing the Anaconda Python distribution

The Anaconda Python distribution contains the Python interpreter, many of the most popular python packages, and several python development environments. Anaconda is available for Windows, MacOS and Linux. The steps in this document are for windows users specifically, but Mac and Linux users will follow a similar procedure.

1. Download the Anaconda installer for **Python 3.9** from <https://www.anaconda.com/download>.  
Anaconda will ask you for your email adress to download a 'cheat cheat'. Feel free to enter your information, but this is **not** necessary. The installer will already have started downloading.
2. Start the downloaded Anaconda installer and follow the instructions.  
   In the advanced options screen make sure that *"Register Anaconda as my default python 3.9"* is selected.  
3. Anaconda will ask you to install Microsoft VS Code. This is a versatile code editor and development enviroment that we will use during the course. When you click install, anaconda will ask for administrator privileges.


## Running the tutorial notebook locally

1. Clone this repository on your local drive using git, or [download the contents as a zip file](https://gitlab.com/novanext/python-course-notebooks/-/archive/master/python-course-notebooks-master.zip).
2. Start Jupyter notebook from your start menu. A terminal window with a local Jupyter notebook server will open, and your webbrowser will be directed to the address of said server.
3. In the Jupyter notebook home screen, browse to the location where you saved the downloaded notebook.
4. Start the notebook by clicking on it. The notebook will launch in a new tab. Enjoy!
